#include <iostream>
#include <conio.h>
#include <string>
#include <queue>
#include <Windows.h>

using namespace std;

// -----------------------------------------------------------------------
// ---------------------------KOLEJKA NA LISCIE---------------------------
template <typename Typ>
class kolejkaList;

template <typename Typ>
class element {
private:
	Typ wartosc;
	element *next; // wskaznik na kolejny element
	element(); // konstruktor
	friend kolejkaList<Typ>;
};

// konstruktor
template <typename Typ>
element<Typ>::element() {
	next = nullptr;
}

template <typename Typ>
class kolejkaList {
public:
	void dodaj(const Typ &wartosc); //funkcja dodajaca element do kolejki
	void wyswietl(); // funkcja wyswietlajaca kolejke
	int rozmiar; // aktualny rozmiar kolejki
	kolejkaList(); // konstruktor
private:
	element<Typ> *first; // wskaznik na pierwszy element kolejki
	element<Typ> *last; // wskaznik na ostatni element kolejki
};

// konstruktor
template <typename Typ>
kolejkaList<Typ>::kolejkaList() {
	first = nullptr;
	last = nullptr;
	rozmiar = 0;
}

template <typename Typ>
void kolejkaList<Typ>::dodaj(const Typ &wartosc)
{
	element<Typ> *nowy = new element<Typ>; // tworzenie nowego elementu
	nowy->wartosc = wartosc;
	if (first == nullptr) { // jesli kolejka jest pusta
		first = nowy;
		last = first;
	}
	else {
		element<Typ> *temp = last;
		last->next = nowy; // ostatni element wskazuje na nowy
		last = nowy; // nowo dodany element jest ostatnim
		last->next = nullptr; // nowy element nie wskazuje na nic
	}
	rozmiar++;
}

template <typename Typ>
void kolejkaList<Typ>::wyswietl()
{
	if (rozmiar == 0) { // gdy lista pusta, wyswietlany jest komunikat
		cout << "Lista jest pusta!" << endl << endl;
	}
	else {
		if (first != nullptr) {
			element<Typ> *temp = first; // wskaznik na pierwszy element listy
			while (temp != nullptr) {
				cout << temp->wartosc << " ";
				temp = temp->next;  // "przelatuje" po kolei po wszystkich elementach
			}
			cout << endl;
		}
	}
}

// ----------------------------------------------------------------------------
// ---------------------------KOLEJKA NA TABLICY-------------------------------
template <typename Typ>
class kolejkaTab {
private:
	Typ *array; // wskaznik na tablice kolejki
	int front; // przod kolejki
	int capacity; // pojemnosc kolejki
	int rear; // tyl kolejki
public:
	void enqueue(const Typ &element); //funkcja dodajaca element na koniec kolejki
	void display(); // funkcja wyswietlajaca kolejke
	void dequeue(); // usuwanie elementu z poczatku kolejki
	void dequeueAll(); // usuwanie wszystkich elementow kolejki
	//Typ& front(); // element z przodu kolejki
	int size(); // aktualny rozmiar kolejki
	bool isEmpty(); // sprawdza, czy kolejka jest pusta
	kolejkaTab(int c); // konstruktor
};

// konstruktor
template <typename Typ>
kolejkaTab<Typ>::kolejkaTab(int c)
{
	capacity = c;
	array = new Typ[capacity]; // poczatkowy rozmiar tablicy
	front = 0; // indeks przedniego elementu
	rear = -1; // indeks ostatniego elementu
}

template <typename Typ>
int kolejkaTab<Typ>::size()
{
	return ((capacity - front + rear + 1) % capacity); // zwraca rozmiar kolejki
}

template <typename Typ>
bool kolejkaTab<Typ>::isEmpty()
{
	return (front - rear == 1); // zwraca true, gdy kolejka jest pusta
}

template <typename Typ>
void kolejkaTab<Typ>::enqueue(const Typ &element)
{
	array[(rear + 1) % capacity] = element; // dodanie elementu na gore stosu
	rear = (rear + 1) % capacity; // gdy dojdzie do konca tablicy, to umieszcza elementy na poczatku jesli to mozliwe
	if ((rear == capacity - 1 && front == 0) || front == rear + 1) { // sprawdzenie, czy tablica jest calkowicie zapelniona
			capacity = capacity * 2; // podwojenie wielkosci tablicy
			Typ *arrayTemp = new Typ[capacity]; // stworzenie tablicy tymczasowej
			int i = 0; // indeks do wypelniania tablicy tymczasowej
			int j = (front % (capacity / 2)); // indeks przedniego elementu
			do {
				j = j % (capacity / 2);
				arrayTemp[i] = array[(j % (capacity / 2))]; // przepisanie elementow do tablicy tymczasowej
				i++;
				j++;
			} while (j != rear + 1); // sprawdzenie, czy funkcja "obeszla" juz cala tablice
			front = 0; // pierwszy element kolejki jest na poczatku tablicy
			rear = capacity / 2 - 1; // nowy indeks ostatniego elementu kolejki
			delete[] array;
			array = arrayTemp; // przypisanie elementow do pierwotnej, powiekszonej tablicy
	}
}

template <typename Typ>
void kolejkaTab<Typ>::dequeue()
{ 
	if (isEmpty())
		cout << "Kolejka jest pusta!\n\n";
	else {
/*
	}
	if (top < capacity / 2 && capacity > 3) { // sprawdzenie, czy tablica jest wypelniona w mniej niz polowie
		capacity = capacity / 2; // dwukrotne zmniejszenie wielkosci tablicy
		Typ *arrayTemp = new Typ[capacity]; // stworzenie tablicy tymczasowej
		for (int j = 0; j <= top; j++)
			arrayTemp[j] = array[j]; // przepisanie elementow do tablicy tymczasowej
		delete[] array;
		array = arrayTemp; // przypisanie elementow do pierwotnej, pomniejszonej tablicy
	} */
		front = (front + 1) % capacity; // przesuniecie przodu kolejki o 1 miejsce dalej
		cout << "Element zostal usuniety" << endl;
	}
}

template <typename Typ>
void kolejkaTab<Typ>::dequeueAll()
{ 
	if (isEmpty())
		cout << "Kolejka jest pusta!\n\n";
	else {
		while (!isEmpty()) // petla dziala dopoki stos nie bedzie pusty
			dequeue();
		cout << "Usunieto wszystkie elementy\n\n";
	} 
}

template <typename Typ>
void kolejkaTab<Typ>::display()
{
	if (isEmpty())
		cout << "Kolejka jest pusta!\n\n";
	else {
		///cout << "tyl: " << rear << " przod: " << front << " pojemnosc: " << capacity << " rozmiar: " << size() << endl;
		int j = (front % capacity); // indeks przedniego elementu
		 do {
			j = j % capacity;
			cout << array[(j % capacity)] << " "; // wypisanie danego elementu
			j++;
		} while (j != rear + 1); // sprawdzenie czy funkcja "obeszla" juz cala tablice
		cout << endl;
	}
}

// funkcja wyswietlajaca kolejke z biblioteki STL
void displaySTL(queue<int> &queueSTL)
{
	queue<int> queueSTLtemp; // tworzenie pomocniczej kolejki
	while (!queueSTL.empty()) {
		queueSTLtemp.push(queueSTL.front()); // przerzucanie elementow z jednej kolejki do drugiej
		queueSTL.pop();
	}
	while (!queueSTLtemp.empty()) {
		cout << queueSTLtemp.front() << " "; // wyswietlanie przedniego elementu
		queueSTL.push(queueSTLtemp.front()); // przerzucanie elementow z pomocniczej kolejki na pierwotna
		queueSTLtemp.pop();
	}
	cout << endl;
}

// funkcja wyswietlajaca menu wyboru stosu
void chooseStack()
{
	cout << "Ktora kolejke chcesz wybrac?\n";
	cout << "1. Kolejka z zastosowaniem implementacji bazujacej na tablicy\n";
	cout << "2. Kolejka z zastosowaniem implementacji w oparciu o biblioteke STL\n";
	cout << "3. Pomiar czasu dla roznego rodzaju kolejek\n";
	cout << "0. Wyjdz z programu\n\n";
}

// funkcja wyswietlajaca menu
void displayMenu(char opcja)
{
	if (opcja == '1')
		cout << "*******KOLEJKA NA TABLICY*******" << endl;
	else if (opcja == '2')
		cout << "*******KOLEJKA STL*******" << endl;
	cout << "-------------MENU-------------" << endl;
	cout << "1. Dodaj element do kolejki" << endl;
	cout << "2. Usun element z kolejki" << endl;
	cout << "3. Wyswietl kolejke" << endl;
	cout << "4. Usun wszystkie elementy z kolejki" << endl;
	cout << "0. Wroc do poprzedniego menu" << endl << endl;
}

int main()
{
	kolejkaTab<int> *queueTab = new kolejkaTab<int>(3);
	queue<int> queueSTL;

	// kolejki do pomiaru czasu
	kolejkaTab<int> *queueTab2 = new kolejkaTab<int>(3);
	kolejkaList<int> *queueList = new kolejkaList<int>;
	queue<int> queueSTL2;

	int doDodania; // liczba, ktora zostanie dodana do kolejki
	char option; // opcja wybierana przez uzytkownika w wewnetrznym menu
	char opcja; // opcja wybierana przez uzytkownika w zewnetrznym menu
	int ileElementow; // ilosc elementow do dodania do kolejki
	LARGE_INTEGER frequency; // czestotliwosc
	LARGE_INTEGER t1, t2; // czas
	double time; // wyliczony czas dzialania algorytmu

	do {
		chooseStack(); // wyswietlenie menu wyboru stosu
		opcja = _getch(); // pobranie znaku
		cout << endl;

		switch (opcja) {

		case '1': // kolejka z zastosowaniem implementacji bazujacej na tablicy
			do {
				displayMenu(opcja); // wyswietlenie menu
				option = _getch(); // pobranie znaku
				cout << endl;
				switch (option) {
				case '1': // dodawanie elementu
					cout << "Jaka liczbe chcesz dodac?\n";
					cin >> doDodania;
					queueTab->enqueue(doDodania);
					break;
				case '2': // usuwanie elementu
					queueTab->dequeue();
					break;
				case '3': // wyswietlanie kolejki
					queueTab->display();
					break;
				case '4': // usuwanie wszystkich elementow
					queueTab->dequeueAll();
					break;
				default: // gdy uzytkownik wybierze nieistniejaca opcje
					if (option != '0')
						cout << "Nie ma takiej opcji!\n\n";
				}
			} while (option != '0'); // powrot do poprzedniego menu, gdy uzytkownik wybierze '0'
			break;

		case '2': // kolejka z zastosowaniem implementacji w oparciu o biblioteke STL
			do { 
				displayMenu(opcja); // wyswietlenie menu
				option = _getch(); // pobranie znaku
				cout << endl; 
				switch (option) {
				case '1': // dodawanie elementu
					cout << "Jaka liczbe chcesz dodac?\n";
					cin >> doDodania;
					queueSTL.push(doDodania);
					break;
				case '2': // usuwanie elementu
					if (queueSTL.empty())
						cout << "Kolejka jest pusta!\n\n";
					else
						queueSTL.pop();
					break;
				case '3': // wyswietlanie kolejki
					if (queueSTL.empty())
						cout << "Kolejka jest pusta!\n\n";
					else
						displaySTL(queueSTL);
					break;
				case '4': // usuwanie wszystkich elementow
					if (queueSTL.empty())
						cout << "Kolejka jest pusta!\n\n";
					else {
						while (!queueSTL.empty())
							queueSTL.pop();
					}
					break;
				default: // gdy uzytkownik wybierze nieistniejaca opcje
					if (option != '0')
						cout << "Nie ma takiej opcji!\n\n";
				} 
			} while (option != '0'); // powrot do poprzedniego menu, gdy uzytkownik wybierze '0'
			break;

		case '3': // pomiar czasu
			cout << "Ile elementow dodac do kolejek? ";
			cin >> ileElementow; // ile elementow ma zostac dodanych do kolejki
			QueryPerformanceFrequency(&frequency); // pobranie czestotliwosci

			QueryPerformanceCounter(&t1); // uruchomienie timera
			for (int i = 0; i < ileElementow; i++)
				queueTab2->enqueue(rand() % 50); // dodawanie losowej liczby
			QueryPerformanceCounter(&t2); // zatrzymanie timera
			time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
			cout << "Czas dla stosu bazujacego na tablicy: " << time << " ms.\n";

			QueryPerformanceCounter(&t1); // uruchomienie timera
			for (int i = 0; i < ileElementow; i++)
				queueSTL2.push(rand() % 50); // dodawanie losowej liczby
			QueryPerformanceCounter(&t2); // zatrzymanie timera
			time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
			cout << "Czas dla stosu bazujacego na bibliotece STL: " << time << " ms.\n";

			QueryPerformanceCounter(&t1); // uruchomienie timera
			for (int i = 0; i < ileElementow; i++)
				queueList->dodaj(rand() % 50); // dodawanie losowej liczby
			QueryPerformanceCounter(&t2); // zatrzymanie timera
			time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
			cout << "Czas dla stosu bazujacego na liscie: " << time << " ms.\n";
			break;
		default: // gdy uzytkownik wybierze nieistniejaca opcje
			if (opcja != '0')
				cout << "Nie ma takiej opcji!\n\n";
		}
	} while (opcja != '0'); // koniec programu, gdy uzytkownik wybierze '0'
}