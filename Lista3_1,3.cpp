#include <iostream>
#include <conio.h>
#include <string>
#include <stack>
#include <Windows.h>

using namespace std;

// -----------------------------------------------------------------------
// ---------------------------STOS NA LISCIE------------------------------
template <typename Typ>
class stosList;

template <typename Typ>
class element {
private:
	Typ wartosc;
	element *next; // wskaznik na kolejny element
	element(); // konstruktor
	friend stosList<Typ>;
};

// konstruktor
template <typename Typ>
element<Typ>::element() {
	next = nullptr;
}

template <typename Typ>
class stosList {
public:
	void dodaj(const Typ &wartosc); //funkcja dodajaca element do stosu
	void wyswietl(); // funkcja wyswietlajaca stos
	int rozmiar; // aktualny rozmiar stosu
	stosList(); // konstruktor
private:
	element<Typ> *first; // wskaznik na pierwszy element stosu
	element<Typ> *last; // wskaznik na ostatni element stosu
};

// konstruktor
template <typename Typ>
stosList<Typ>::stosList() {
	first = nullptr;
	last = nullptr;
	rozmiar = 0;
}

template <typename Typ>
void stosList<Typ>::dodaj(const Typ &wartosc)
{
	element<Typ> *nowy = new element<Typ>; // tworzenie nowego elementu
	nowy->wartosc = wartosc;
	if (first == nullptr) { // jesli stos jest pusty
		first = nowy;
		last = first;
	}
	else {
		element<Typ> *temp = last;
		last->next = nowy; // ostatni element wskazuje na nowy
		last = nowy; // nowo dodany element jest ostatnim
		last->next = nullptr; // nowy element nie wskazuje na nic
	}
	rozmiar++;
}

template <typename Typ>
void stosList<Typ>::wyswietl()
{
	if (rozmiar == 0) { // gdy lista pusta, wyswietlany jest komunikat
		cout << "Lista jest pusta!" << endl << endl;
	}
	else {
		if (first != nullptr) {
			element<Typ> *temp = first; // wskaznik na pierwszy element listy
			while (temp != nullptr) {
				cout << temp->wartosc << " ";
				temp = temp->next;  // "przelatuje" po kolei po wszystkich elementach
			}
			cout << endl;
		}
	}
}

// ----------------------------------------------------------------------------
// ---------------------------STOS NA TABLICY----------------------------------

template <typename Typ>
class stosTab {
private:
	Typ *array; // wskaznik na tablice stosu
	int top; // gora stosu
	int capacity; // pojemnosc stosu
public:
	void push(const Typ &element); //funkcja dodajaca element na gore stosu
	void display(); // funkcja wyswietlajaca stos
	void pop(); // usuwanie elementu z gory stosu
	void popAll(); // usuwanie wszystkich elementow stosu
	//Typ& top(); // element na gorze stosu
	int size(); // aktualny rozmiar stosu
	bool isEmpty(); // sprawdza czy stos jest pusty
	stosTab(int c); // konstruktor

};

// konstruktor
template <typename Typ>
stosTab<Typ>::stosTab(int c) 
{
	capacity = c;
	array = new Typ[capacity]; // poczatkowy rozmiar tablicy
	top = -1; // indeks gornego elementu
}

template <typename Typ>
int stosTab<Typ>::size()
{
	return (top + 1); // zwraca rozmiar stosu
}

template <typename Typ>
bool stosTab<Typ>::isEmpty()
{
	return (top < 0); // zwraca true, gdy stos jest pusty
}

template <typename Typ>
void stosTab<Typ>::push(const Typ &element) 
{
	if (top == capacity - 1) { // sprawdzenie, czy tablica jest calkowicie zapelniona
		capacity = capacity * 2; // podwojenie wielkosci tablicy
		///capacity = capacity + 500; // powiekszenie tablicy o 100 elementow
		Typ *arrayTemp = new Typ[capacity]; // stworzenie tablicy tymczasowej
		for (int j = 0; j <= top; j++)
			arrayTemp[j] = array[j]; // przepisanie elementow do tablicy tymczasowej
		delete[] array;
		array = arrayTemp; // przypisanie elementow do pierwotnej, powiekszonej tablicy
	}
	array[top + 1] = element; // dodanie elementu na gore stosu
	top++;
}

template <typename Typ>
void stosTab<Typ>::pop()
{
	if (isEmpty())
		cout << "Stos jest pusty!\n\n";
	else {
		top--; // indeks elementu na gorze stosu jest o 1 mniejszy
		cout << "Element zostal usuniety\n\n";
	}
	if (top < capacity / 2 && capacity > 3) { // sprawdzenie, czy tablica jest wypelniona w mniej niz polowie
		capacity = capacity / 2; // dwukrotne zmniejszenie wielkosci tablicy
		Typ *arrayTemp = new Typ[capacity]; // stworzenie tablicy tymczasowej
		for (int j = 0; j <= top; j++)
			arrayTemp[j] = array[j]; // przepisanie elementow do tablicy tymczasowej
		delete[] array;
		array = arrayTemp; // przypisanie elementow do pierwotnej, pomniejszonej tablicy
	}
}

template <typename Typ>
void stosTab<Typ>::popAll()
{
	if (isEmpty())
		cout << "Stos jest pusty!\n\n";
	else {
		while (!isEmpty()) // petla dziala dopoki stos nie bedzie pusty
			pop();
		cout << "Usunieto wszystkie elementy\n\n";
	}
}

template <typename Typ>
void stosTab<Typ>::display()
{
	if (isEmpty())
		cout << "Stos jest pusty!\n\n";
	else {
		///cout << top << "    " << capacity << endl;
		for (int i = 0; i <= top; i++)
			cout << array[i] << " ";
		cout << endl;
	}
}

// funkcja wyswietlajaca stos z biblioteki STL
void displaySTL(stack<int> &stackSTL)
{
	stack<int> stackSTLtemp; // tworzenie pomocniczego stosu
	while (!stackSTL.empty()) {
		stackSTLtemp.push(stackSTL.top()); // przerzucanie elementow z jednego stosu na drugi
		stackSTL.pop();
	}
	while (!stackSTLtemp.empty()) {
		cout << stackSTLtemp.top() << " "; // wyswietlanie gornego elementu
		stackSTL.push(stackSTLtemp.top()); // przerzucanie elementow z pomocniczego stosu na pierwotny 
		stackSTLtemp.pop();
	}
	cout << endl;
}

// funkcja wyswietlajaca menu wyboru stosu
void chooseStack()
{
	cout << "Ktory stos chcesz wybrac?\n";
	cout << "1. Stos z zastosowaniem implementacji bazujacej na tablicy\n";
	cout << "2. Stos z zastosowaniem implementacji w oparciu o biblioteke STL\n";
	cout << "3. Pomiar czasu dla roznego rodzaju stosu\n";
	cout << "0. Wyjdz z programu\n\n";
}

// funkcja wyswietlajaca menu
void displayMenu(char opcja)
{
	if (opcja == '1')
		cout << "********STOS NA TABLICY********" << endl;
	else if (opcja == '2')
		cout << "********STOS STL********" << endl;
	cout << "-------------MENU-------------" << endl;
	cout << "1. Dodaj element do stosu" << endl;
	cout << "2. Usun element ze stosu" << endl;
	cout << "3. Wyswietl stos" << endl;
	cout << "4. Usun wszystkie elementy ze stosu" << endl;
	cout << "0. Wroc do poprzedniego menu" << endl << endl;
}

int main()
{
	stosTab<int> *stackTab = new stosTab<int>(3);
	stack<int> stackSTL;

	// stosy do pomiaru czasu
	stosTab<int> *stackTab2 = new stosTab<int>(3);
	stosList<int> *stackList = new stosList<int>;
	stack<int> stackSTL2;

	int doDodania; // liczba, ktora zostanie dodana na stos
	char option; // opcja wybierana przez uzytkownika w wewnetrznym menu
	char opcja; // opcja wybierana przez uzytkownika w zewnetrznym menu
	int ileElementow; // ilosc elementow do dodania na stos
	LARGE_INTEGER frequency; // czestotliwosc
	LARGE_INTEGER t1, t2; // czas
	double time; // wyliczony czas dzialania algorytmu

	do {
		chooseStack(); // wyswietlenie menu wyboru stosu
		opcja = _getch(); // pobranie znaku
		cout << endl;

		switch (opcja) {

		case '1': // stos z zastosowaniem implementacji bazujacej na tablicy
			do {
				displayMenu(opcja); // wyswietlenie menu
				option = _getch(); // pobranie znaku
				cout << endl;
				switch (option) {
				case '1': // dodawanie elementu
					cout << "Jaka liczbe chcesz dodac?\n";
					cin >> doDodania;
					stackTab->push(doDodania);
					break;
				case '2': // usuwanie elementu
					stackTab->pop();
					break;
				case '3': // wyswietlanie kolejki
					stackTab->display();
					break;
				case '4': // usuwanie wszystkich elementow
					stackTab->popAll();
					break;
				default: // gdy uzytkownik wybierze nieistniejaca opcje
					if (option != '0')
						cout << "Nie ma takiej opcji!\n\n";
				}
			} while (option != '0'); // powrot do poprzedniego menu, gdy uzytkownik wybierze '0'
			break;

		case '2': // stos z zastosowaniem implementacji w oparciu o biblioteke STL
			do {
				displayMenu(opcja); // wyswietlenie menu
				option = _getch(); // pobranie znaku
				cout << endl;
				switch (option) {
				case '1': // dodawanie elementu
					cout << "Jaka liczbe chcesz dodac?\n";
					cin >> doDodania;
					stackSTL.push(doDodania);
					break;
				case '2': // usuwanie elementu
					if (stackSTL.empty())
						cout << "Stos jest pusty!\n\n";
					else
						stackSTL.pop();
					break;
				case '3': // wyswietlanie kolejki
					if (stackSTL.empty())
						cout << "Stos jest pusty!\n\n";
					else
						displaySTL(stackSTL);
					break;
				case '4': // usuwanie wszystkich elementow
					if (stackSTL.empty())
						cout << "Stos jest pusty!\n\n";
					else {
						while (!stackSTL.empty())
							stackSTL.pop();
					}
					break;
				default: // gdy uzytkownik wybierze nieistniejaca opcje
					if (option != '0')
						cout << "Nie ma takiej opcji!\n\n";
				}
			} while (option != '0'); // powrot do poprzedniego menu, gdy uzytkownik wybierze '0'
			break;

		case '3': // pomiar czasu
			cout << "Ile elementow dodac do stosow? ";
			cin >> ileElementow;
			QueryPerformanceFrequency(&frequency); // pobranie czestotliwosci
			
			QueryPerformanceCounter(&t1); // uruchomienie timera
			for (int i = 0; i < ileElementow; i++)
				stackTab2->push(rand() % 50); // dodawanie losowej liczby
			QueryPerformanceCounter(&t2); // zatrzymanie timera
			time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
			cout << "Czas dla kolejki bazujacej na tablicy: " << time << " ms.\n";

			QueryPerformanceCounter(&t1); // uruchomienie timera
			for (int i = 0; i < ileElementow; i++)
				stackSTL2.push(rand() % 50); // dodawanie losowej liczby
			QueryPerformanceCounter(&t2); // zatrzymanie timera
			time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
			cout << "Czas dla kolejki bazujacej na bibliotece STL: " << time << " ms.\n";

			QueryPerformanceCounter(&t1); // uruchomienie timera
			for (int i = 0; i < ileElementow; i++)
				stackList->dodaj(rand() % 50); // dodawanie losowej liczby
			QueryPerformanceCounter(&t2); // zatrzymanie timera
			time = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart; // obliczenie czasu
			cout << "Czas dla kolejki bazujacej na liscie: " << time << " ms.\n";
			break;
		default: // gdy uzytkownik wybierze nieistniejaca opcje
			if (opcja != '0')
				cout << "Nie ma takiej opcji!\n\n";
		}
	} while (opcja != '0'); // koniec programu, gdy uzytkownik wybierze '0'
}