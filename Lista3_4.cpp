#include <iostream>
#include <stdlib.h>

using namespace std;

template <typename Typ>
class stosTab {
private:
	Typ *array; // wskaznik na tablice stosu
	int top; // gora stosu
	int capacity; // pojemnosc stosu
public:
	void push(const Typ &element); //funkcja dodajaca element na gore stosu
	void display(); // funkcja wyswietlajaca stos
	void pop(); // usuwanie elementu z gory stosu
	void popAll(); // usuwanie wszystkich elementow stosu
	Typ& Top(); // element na gorze stosu
	int size(); // aktualny rozmiar stosu
	bool isEmpty(); // sprawdza czy stos jest pusty
	stosTab(int c, int k); // konstruktor
	int ktoryStos;
};

template <typename Typ>
Typ& stosTab<Typ>::Top()
{
	return array[top];
}

// konstruktor
template <typename Typ>
stosTab<Typ>::stosTab(int c, int k)
{
	ktoryStos = k;
	capacity = c;
	array = new Typ[capacity]; // poczatkowy rozmiar tablicy
	top = -1; // indeks gornego elementu
}

template <typename Typ>
int stosTab<Typ>::size()
{
	return (top + 1); // zwraca rozmiar stosu
}

template <typename Typ>
bool stosTab<Typ>::isEmpty()
{
	return (top < 0); // zwraca true, gdy stos jest pusty
}

template <typename Typ>
void stosTab<Typ>::push(const Typ &element)
{
	if (top == capacity - 1) { // sprawdzenie, czy tablica jest calkowicie zapelniona
		capacity = capacity * 2; // podwojenie wielkosci tablicy
		Typ *arrayTemp = new Typ[capacity]; // stworzenie tablicy tymczasowej
		for (int j = 0; j <= top; j++)
			arrayTemp[j] = array[j]; // przepisanie elementow do tablicy tymczasowej
		delete[] array;
		array = arrayTemp; // przypisanie elementow do pierwotnej, powiekszonej tablicy
	}
	array[top + 1] = element; // dodanie elementu na gore stosu
	top++;
}

template <typename Typ>
void stosTab<Typ>::pop()
{
	if (isEmpty())
		cout << "Stos jest pusty!\n\n";
	else {
		top--; // indeks elementu na gorze stosu jest o 1 mniejszy
		///cout << "Element zostal usuniety\n\n";
	}
	if (top < capacity / 2 && capacity > 3) { // sprawdzenie, czy tablica jest wypelniona w mniej niz polowie
		capacity = capacity / 2; // dwukrotne zmniejszenie wielkosci tablicy
		Typ *arrayTemp = new Typ[capacity]; // stworzenie tablicy tymczasowej
		for (int j = 0; j <= top; j++)
			arrayTemp[j] = array[j]; // przepisanie elementow do tablicy tymczasowej
		delete[] array;
		array = arrayTemp; // przypisanie elementow do pierwotnej, pomniejszonej tablicy
	}
}

template <typename Typ>
void stosTab<Typ>::popAll()
{
	if (isEmpty())
		///cout << "Stos jest pusty!\n\n";
	else {
		while (!isEmpty()) // petla dziala dopoki stos nie bedzie pusty
			pop();
		cout << "Usunieto wszystkie elementy\n\n";
	}
}

template <typename Typ>
void stosTab<Typ>::display()
{
	//if (isEmpty())
		//cout << "Stos jest pusty!\n";
	//else {
		//cout << top << "    " << capacity << endl;
	if (ktoryStos == 0)
		cout << "Stos pierwotny: ";
	else if (ktoryStos == 1)
		cout << "Stos wyjsciowy: ";
	else if (ktoryStos == 2)
		cout << "Stos pomocniczy: ";
	if (isEmpty())
		cout << "Stos jest pusty!\n";
	else {
		for (int i = 0; i <= top; i++)
			cout << array[i] << " ";
		cout << endl;
	}

}

// funkcja przenoszaca element z jednego stosu na drugi
template <typename Typ>
void move(stosTab<Typ> *fromStack, stosTab<Typ> *toStack)
{
	toStack->push(fromStack->Top()); // dodaje na stos toStack element z gory stosu fromStack
	//cout << "Top: " << fromStack->Top() << endl;
	fromStack->pop(); // usuwa gorny element z fromStack
}

// funkcja rekurencyjna wykonujaca zadanie przeniesienia wiezy z jednego stosu na drugi
template <typename Typ>
void hanoi(int ilosc, stosTab<Typ> *fromStack, stosTab<Typ> *toStack, stosTab<Typ> *withStack, int &numerKroku, int &ktoryKrok)
{
	if (ilosc >= 1)
	{	
		hanoi(ilosc - 1, fromStack, withStack, toStack, numerKroku, ktoryKrok); // rekurencyjne wywolanie funkcji z iloscia krazkow pomniejszona o 1 i zamiana stosow, by z pierwotnego przenosilo na pomocniczy
		if (ktoryKrok == numerKroku) {
			cout << "1. StosA: "; fromStack->display(); cout << "2. StosB: "; toStack->display(); cout << "3. StosC: "; withStack->display();
			system("PAUSE");
		}
		move(fromStack, toStack); // przeniesienie elementu z fromStack na toStack
		numerKroku++;
		hanoi(ilosc - 1, withStack, toStack, fromStack, numerKroku, ktoryKrok); // rekurencyjne wywolanie funkcji z iloscia krazkow pomniejszona o 1 i zamiana stosow, by z pomocniczego przenosilo na docelowy
	}
}

int main()
{
	stosTab<int> *stackA = new stosTab<int>(3, 0); // stos na ktorym pierwotnie beda umieszczone krazki, 0 oznacza stos pierwotny
	stosTab<int> *stackB = new stosTab<int>(3, 1); // stos na ktory przeniesione zostana krazki, 1 oznacza stos wyjsciowy
	stosTab<int> *stackC = new stosTab<int>(3, 2); // stos pomocniczy do przenoszenia krazkow, 2 oznacza stos pomocniczy
	int liczba; // liczba krazkow na wiezy hanoi
	int ktoryKrok; // krok w ktorym program ma sie zatrzymac 
	int numerKroku = 0; // numer kroku na ktorym jest program
	cout << "Ile krazkow umiescic na wiezy? ";
	cin >> liczba;
	for (int i = liczba; i > 0; i--)
		stackA->push(i); // dodanie elementow na wieze Hanoi
	cout << "StosA: "; stackA->display(); cout << "StosB: "; stackB->display(); cout << "StosC: "; stackC->display(); cout << endl;
	cout << "W ktorym kroku sie zatrzymac?" << endl;
	cin >> ktoryKrok;
	hanoi(liczba, stackA, stackB, stackC, numerKroku, ktoryKrok);
	cout << "\n\nStosA: "; stackA->display(); cout << "StosB: "; stackB->display(); cout << "StosC: "; stackC->display(); cout << endl;
}